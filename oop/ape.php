<?php
class Ape extends Animal
{
  public $legs = 2;
  public function __construct($name)
  {
    echo "Name: " . $this->name = $name . "<br>";
    echo "Legs: " . $this->legs . "<br>";
    echo "Cold Blooded: " . $this->cold_blooded . "<br>";
  }
  public function yell()
  {
    echo "Yell: Auooo <br><br>";
  }
}
