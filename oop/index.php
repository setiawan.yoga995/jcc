<?php
require 'animal.php';
require 'frog.php';
require 'ape.php';

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded; // "no"
echo "<br><br>";

$sungokong = new Ape("Kera Sakti");
$sungokong->yell();
$kodok = new Frog("Buduk");
$kodok->jump();
