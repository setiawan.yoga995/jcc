<?php
class Frog extends Animal
{

  public function __construct($name)
  {
    echo "Name: " . $this->name = $name . "<br>";
    echo "Legs: " . $this->legs . "<br>";
    echo "Cold Blooded: " . $this->cold_blooded . "<br>";
  }
  public function jump()
  {
    echo "Jump: Hop Hop <br>";
  }
}
